package com.example.clevertek_ex_4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Toast

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.clevertek_ex_4.databinding.ActivityMapsBinding
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Math.abs
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timer
import kotlin.math.sqrt

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    val my_locationX = 52.425163
    val my_locationY = 31.015039



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)



    }


//    private suspend fun main() = coroutineScope {
//        val listATM: Deferred<ArrayList<pointInfoElement>> = async { getFullInfoATM() }
//        val listInfobox: Deferred<ArrayList<pointInfoElement>> = async { getFullInfoInfobox() }
//        val listFillials: Deferred<ArrayList<pointInfoElement>> = async { getFullInfoFillials() }
//        displayPoints(listATM.await(), listInfobox.await(), listFillials.await())
//    }



    override fun onMapReady(googleMap: GoogleMap) {
        val gomel = LatLng(52.431235, 30.992704)
        val myPos = LatLng(my_locationX, my_locationY)
        mMap = googleMap
        mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
        mMap.setMinZoomPreference(9.0f)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 15f))
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10f), 2000, null)
        val cameraPosition = CameraPosition.Builder()
            .target(myPos)
            .zoom(15f)
            .build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        mMap.addMarker(MarkerOptions().position(myPos).title("I'm here"))
        CoroutineScope(Dispatchers.Default).launch {
            main()
        }


//


//        val listATM = getFullInfoATM()
//        val listInfobox = getFullInfoInfobox()
//        val listFillials = getFullInfoFillials()
//        android.os.Handler().postDelayed({ displayPoints(listATM, listFillials, listInfobox) }, 2000)

    }

    suspend fun main() = coroutineScope {
//        val job = async {
//            val listATM = getFullInfoATM()
//            val listInfobox = getFullInfoInfobox()
//            val listFillials = getFullInfoFillials()
//        }
        val listATM = async { getFullInfoATM() }
        val listInfobox = async { getFullInfoInfobox() }
        val listFillials = async { getFullInfoFillials() }
        listATM.join()
        listInfobox.join()
        listFillials.join()
        val asd = listATM.await()
        val aaa = listFillials.await()
        val sss = listInfobox.await()
//        Log.e("aaaa", "len1 ${asd.size}")
//        Log.e("aaaa", "len2 ${aaa.size}")
//        Log.e("aaaa", "len3 ${sss.size}")

        displayPoints(asd, aaa, sss)





    }



    suspend fun getFullInfoATM(): ArrayList<pointInfoElement> {
        val listAllPointsATM = arrayListOf<pointInfoElement>()
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(BBRequest::class.java)
        val dataFromServer = retrofitBuilder.getFUllDataATM()
        Log.e("aaaa", "ATM")
        dataFromServer.enqueue(object : Callback<List<ATMFullInfoClass>?> {
            override fun onResponse(
                call: Call<List<ATMFullInfoClass>?>,
                response: Response<List<ATMFullInfoClass>?>) {
                val responseBody = response.body()!!
                Log.e("aaaa", "ATM") 
                for (Atm in responseBody) {
                    Log.e("aaaa", "StartATM")
                    val xGPS = Atm.gps_x.toDouble()
                    val yGPS = Atm.gps_y.toDouble()
                    val place = "ATM " + Atm.ATM_type + " " + Atm.install_place
                    val distance = distance(my_locationX, xGPS, my_locationY, yGPS)
                    val point = pointInfoElement(xGPS, yGPS, distance, place, 20.5F)
//                    mMap.addMarker(MarkerOptions().position(LatLng(xGPS,yGPS)).icon(
//                        BitmapDescriptorFactory
//                            .defaultMarker(point.color)))
                    listAllPointsATM.add(point)
                    Log.e("aaaa", "StopATM")
                }
            }

            override fun onFailure(call: Call<List<ATMFullInfoClass>?>, t: Throwable) {
                Log.e("aaaa", "Ёбнуло")
                Toast.makeText(this@MapsActivity, "УПС...", Toast.LENGTH_SHORT).show()
            }
        })
        return listAllPointsATM
    }

    suspend fun getFullInfoInfobox(): ArrayList<pointInfoElement> {
        val listAllPoints = arrayListOf<pointInfoElement>()
        val retrofitBuilderInfobox = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(BBRequest::class.java)
        val dataFromServer = retrofitBuilderInfobox.getFullDataInfobox()

        dataFromServer.enqueue(object : Callback<List<InfoboxFullInfoClass>?> {
            override fun onResponse(
                call: Call<List<InfoboxFullInfoClass>?>,
                response: Response<List<InfoboxFullInfoClass>?>) {
                val responseBodyInfobox = response.body()!!

                for (infobox in responseBodyInfobox) {
                    val xGPS = infobox.gps_x.toDouble()
                    val yGPS = infobox.gps_y.toDouble()
                    val place = "INFOBOX "+infobox.inf_type+" "+infobox.install_place
                    val distance = distance(my_locationX,xGPS,my_locationY,yGPS)
                    val point = pointInfoElement(xGPS, yGPS, distance, place, 80.5F)
//                    mMap.addMarker(MarkerOptions().position(LatLng(xGPS,yGPS)).icon(
//                        BitmapDescriptorFactory
//                            .defaultMarker(point.color)))
                    listAllPoints.add(point)
                }
            }
            override fun onFailure(call: Call<List<InfoboxFullInfoClass>?>, t: Throwable) {
                Toast.makeText(this@MapsActivity, "УПС...", Toast.LENGTH_SHORT).show()
            }
        })
        return listAllPoints
    }

    suspend fun getFullInfoFillials(): ArrayList<pointInfoElement> {
        val listAllPointsFillials = arrayListOf<pointInfoElement>()
        val retrofitBuilderFillials = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(BBRequest::class.java)
        val dataFromServer = retrofitBuilderFillials.getFullDataFilials()

        dataFromServer.enqueue(object : Callback<List<FilialsFullInfoClass>?>{
            override fun onResponse(
                call: Call<List<FilialsFullInfoClass>?>,
                response: Response<List<FilialsFullInfoClass>?>) {
                val responseBodyInfobox = response.body()!!

                for (fillials in responseBodyInfobox) {
                    val xGPS = fillials.GPS_X.toDouble()
                    val yGPS = fillials.GPS_Y.toDouble()
                    val distance = distance(my_locationX,xGPS,my_locationY,yGPS)
                    val point = pointInfoElement(xGPS, yGPS, distance, "Fillial", 190.5F)
//                    mMap.addMarker(MarkerOptions().position(LatLng(xGPS,yGPS)).icon(
//                        BitmapDescriptorFactory
//                            .defaultMarker(point.color)))
                    listAllPointsFillials.add(point)
                }
            }
            override fun onFailure(call: Call<List<FilialsFullInfoClass>?>, t: Throwable) {
                Toast.makeText(this@MapsActivity, "УПС...", Toast.LENGTH_SHORT).show()
            }
        })
        return listAllPointsFillials
    }

    fun distance(x1: Double, x2: Double, y1: Double, y2: Double): Double {
        val a = abs(x1-x2)
        val b = abs(y1-y2)
        return sqrt(a+b)
    }


    suspend fun displayPoints(
        list1: ArrayList<pointInfoElement>,
        list2: ArrayList<pointInfoElement>,
        list3: ArrayList<pointInfoElement>
    ) {
        Log.e("aaaa", "list1 ${list1.size.toString()}")
        Log.e("aaaa", "list2 ${list2.size.toString()}")
        Log.e("aaaa", "list3 ${list3.size.toString()}")
        list1.addAll(list2)
        list1.addAll(list3)
        list1.sortBy { it.distance }
        for (i in 0..10) {
            val point = list1[i]
            val pos = LatLng(point.x, point.y)
            val color = point.color
            mMap.addMarker(
                MarkerOptions().position(pos)
                    .title(point.place)
                    .icon(
                        BitmapDescriptorFactory
                            .defaultMarker(color)
                    )
            )
        }
        Toast.makeText(this, "Len all: " + list1.size, Toast.LENGTH_SHORT).show()
    }
}